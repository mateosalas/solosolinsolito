<?php

$error = false;

//Nombre
if(empty($_POST['nombre'])){
	$error['nombre'] = "Nombre requerido";
}

//Apellido
if(empty($_POST['apellido'])){
	$error['apellido'] = "Apellido requerido";
}

//celular
if(empty($_POST['celular'])){
	$error['celular'] = "Celular requerido";
}

//Mail
$pos_arroba = strpos($_POST['mail'],'@');
$cant_arrobas = substr_count($_POST['mail'],'@');
if($pos_arroba < 4 || $cant_arrobas != 1){
	$error['mail'] = "Falta @ o mail";
}

//Usuario
if(empty($_POST['usuario'])){
	$error['usuario'] = 'Usuario requerido';
}

//Clave
if(empty($_POST['clave'])){
	$error['clave'] = 'Clave requerida';
}
if(empty($_POST['clave2'])){
	$error['clave2'] = 'Confirmar requerida';
}

//Clave identicas
if(!($_POST['clave']==$_POST['clave2'])){
	$error['clave'] = "Las contraseñas no identicas.";
	$error['clave2'] = "Las contraseñas no identicas.";
}

//Usuario o mail duplicado
$sql_duplicado = "SELECT id 
					FROM estudiantes 
					WHERE usuario = '".$_POST['usuario']."' 
					OR mail = '".$_POST['mail']."'";
					
$res_duplicado = ejecutar_query($conex, $sql_duplicado);

$num_encontrado = mysqli_num_rows($res_duplicado);

if($num_encontrado!=0){
	$error['usuario'] = "usuario o mail duplicado";
}

?>
