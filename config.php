<?php
define('MYSQL_SERVER','localhost');
define('MYSQL_DB','usubuffet');
define('MYSQL_USER','root');
define('MYSQL_PASS','');

$conex= mysqli_connect(MYSQL_SERVER,MYSQL_USER,MYSQL_PASS,MYSQL_DB);

if (!$conex) {
    die('Error de Conexión (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

function ejecutar_query($con, $sql){
	$res = mysqli_query($con,$sql);
	if($res===false){
		die(mysqli_error($con));
	}	
	return $res;
}

?>
