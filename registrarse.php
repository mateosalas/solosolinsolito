<?php
	require_once "config.php";

	if(count($_POST)>0){
		require_once "verificar.php";
		if($error == false){	
			$sql_insert = "INSERT INTO estudiantes 
							(id, nombre, apellido, celular, email, usuario, clave) 
							VALUES 
							(NULL, 
							'".$_POST['nombre']."', 
							'".$_POST['apellido']."', 
							'".$_POST['celular']."', 
							'".$_POST['email']."', 
							'".$_POST['usuario']."', 
							'".$_POST['clave']."')";
			ejecutar_query($con, $sql_insert);
			header('Location: login.php');
		}
	}
?>
<!DOCTYPE html>
<html>	
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
		<title>Registrate aca!!SKEREEEEEE</title>
		<style>
			.estilo_error_input {
				border: 2px solid red;
				border-radius: 4px;
			}
		</style>
	</head>
	<body>
		<form action="" method="POST" >
		<table border='1'>
			<tr>
				<td>
					<label for="nombre">Nombre</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['nombre'])) ? "estilo_error_input" : "" ?>" name="nombre" value="<?php if (isset($_POST['nombre'])) echo $_POST['nombre']; ?>">
					<br>
					<?php echo (isset($error['nombre'])) ? $error['nombre'] : "" ?>				
				</td>
			</tr>
			<tr>
				<td>
					<label for="apellido">Apellido</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['apellido'])) ? "estilo_error_input" : "" ?>"  name="apellido" value="<?php if (isset($_POST['apellido'])) echo $_POST['apellido']; ?>">
					<br>
					<?php echo (isset($error['apellido'])) ? $error['apellido'] : "" ?>
				</td>
			</tr>
			<tr>
				<td>
					<label for="celular">Celular</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['celular'])) ? "estilo_error_input" : "" ?>" name="celular" value="<?php if (isset($_POST['celular'])) echo $_POST['celular']; ?>">
					<br>
					<?php echo (isset($error['celular'])) ? $error['celular'] : "" ?>				
				</td>
			</tr>
			<tr>
				<td>
					<label for="email">Email</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['email'])) ? "estilo_error_input" : "" ?>" name="email" value='<?php echo (isset($_POST['email'])) ? $_POST['email'] : "" ?>'><br>
					<?php echo (isset($error['email'])) ? $error['email'] : "" ?>
					<br>							
				</td>
			</tr>
			<tr>
				<td>
					<label for="usuario">Usuario</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['usuario'])) ? "estilo_error_input" : "" ?>" name="usuario" value="<?php if (isset($_POST['usuario'])) echo $_POST['usuario']; ?>">
					<br>
					<?php echo (isset($error['usuario'])) ? $error['usuario'] : "" ?>				
				</td>
			</tr>
			<tr>
				<td>
					<label for="clave">Clave</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['clave'])) ? "estilo_error_input" : "" ?>" name="clave" type="password">
					<br>
					<?php echo (isset($error['clave'])) ? $error['clave'] : "" ?>								
				</td>
			</tr>
			<tr>
				<td>
					<label for="clave2">Confirm Clave</label>
				</td>
				<td>
					<input class="<?php echo (isset($error['clave2'])) ? "estilo_error_input" : "" ?>" name="clave2" type="password">
					<br>
					<?php echo (isset($error['clave2'])) ? $error['clave2'] : "" ?>
				</td>
			</tr>
			<tr>
				<td colspan='2'>
					<input type="submit">
				</td>
			</tr>
		</table>
		</form>
	</body>
</html>
